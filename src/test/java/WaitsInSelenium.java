import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WaitsInSelenium {

	public static void main(String[] args) {

		// Implicit 
		// Explicit
		// Fluent 
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir")+"\\src\\test\\resources\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.testfire.net/login.jsp");
		
		driver.manage().timeouts().implicitlyWait(50000, TimeUnit.MILLISECONDS);
		
		driver.findElement(By.id("uid")).sendKeys("jsmith");
		driver.findElement(By.id("password")).sendKeys("demo1234");
		
		
		
	}
}
