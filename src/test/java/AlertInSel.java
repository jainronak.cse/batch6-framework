import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertInSel {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir")+"\\src\\test\\resources\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/alerts");
//		driver.findElement(By.id("alertButton")).click();
//		Alert al=driver.switchTo().alert();
//		System.out.println(al.getText());
//		al.accept();
		
//		driver.findElement(By.id("confirmButton")).click();
//		Alert al=driver.switchTo().alert();
//		System.out.println(al.getText());
//		al.accept();
//		String s=driver.findElement(By.id("confirmResult")).getText();
//		System.out.println(s);
//		driver.findElement(By.id("confirmButton")).click();
//		 al=driver.switchTo().alert();
//		System.out.println(al.getText());
//		al.dismiss();
//		 s=driver.findElement(By.id("confirmResult")).getText();
//			System.out.println(s);
		
		driver.findElement(By.id("promtButton")).click();
		Alert al=driver.switchTo().alert();
		System.out.println(al.getText());
		al.sendKeys("rounak");
		al.accept();
		String s=driver.findElement(By.id("promptResult")).getText();
		System.out.println(s);
		
		
	}
}
