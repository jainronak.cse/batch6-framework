package PageClasses;

import org.openqa.selenium.By;

import Base.BaseClass;

public class LoginPage  {

//	By uname = By.id("input-email");
//	By pwd = By.id("input-password");
//	By login = By.xpath("//input[@value='Login']");
//	By signOff = By.xpath("(//a[text()='Logout'])[2]");
	By warningMsg = By.xpath("//div[contains(text(),'No match for E-Mail Address and/or Password')]");

// Altoromutual
	By uname = By.id("uid");
	By pwd = By.id("passw");
	By login = By.name("btnSubmit");
	By signOff = By.xpath("//font[text()='Sign Off']");
	
	
	
	public void enterUserName(String userName) {
		BaseClass.bb.sendKeys(uname, userName);
	}

	public void enterPassword(String password) {
		BaseClass.bb.sendKeys(pwd, password);
	}

	public void clickSubmit() {
		BaseClass.bb.click(login);
	}

	public boolean verifyLogin() {
		return BaseClass.bb.isDisplayed(signOff);
	}

	public void login(String userName, String password) {
		enterUserName(userName);
		enterPassword(password);
		clickSubmit();
		new HomePage();
	}

	public boolean verifyWarning() {
		return BaseClass.bb.isDisplayed(warningMsg);
	}
}
