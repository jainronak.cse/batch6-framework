package Utilities;

import java.util.List;

import javax.crypto.SealedObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import Base.BaseClass;

public class BrowserBasics extends BaseClass {

	public void sendKeys(By locator, String value) {
		getDriver().findElement(locator).sendKeys(value);
	}

	public void click(By locator) {
		getDriver().findElement(locator).click();
	}

	public boolean isDisplayed(By locator) {
		boolean flag = false;
		try {
			flag = getDriver().findElement(locator).isDisplayed();

		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}
	
	public List<WebElement> getDDOptions(By locator) {
		Select sel = new Select(getDriver().findElement(locator));
		return sel.getOptions();
	}
	public void selectByIndex(By locator,int index) {
		Select sel = new Select(getDriver().findElement(locator));
		sel.selectByIndex(index);
	}
	
	
	

}
