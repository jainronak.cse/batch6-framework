package Utilities;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitUtility {
	private WebDriver driver;
	WebDriverWait wait ;
	public WaitUtility(WebDriver driver){
		this.driver=driver;
		wait = new WebDriverWait(this.driver, 3000);
		
	}
	
	
	public Alert alertIsPresent() {
		Alert al =wait.until(ExpectedConditions.alertIsPresent());
		
		return al;
	}
}
