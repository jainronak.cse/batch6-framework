package Utilities;

import org.apache.log4j.Logger;

public class LoggerUtility {

	public static Logger log= Logger.getLogger(LoggerUtility.class);
	
	public void logDebug(String message) {
		log.debug(message);
	}
}
