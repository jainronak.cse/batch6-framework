package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtility {
	File f;

	public ExcelUtility(String excelPath) {
		f = new File(excelPath);
		try {
			FileInputStream fis = new FileInputStream(f);
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
	}

	// .xls : HSSFWorkbook
	// .xlsx : XSSFWorkbook

	public String returnCellData(String shtName, int rowNo, int colNo) throws InvalidFormatException, IOException {

		XSSFWorkbook wb = new XSSFWorkbook(f);
		XSSFSheet ws = wb.getSheet(shtName);
		Row r = ws.getRow(rowNo);
		Cell c = r.getCell(colNo);

		String s = c.getStringCellValue();
		return s;
	}

	public Object[][] returnAllData(String shtName) throws InvalidFormatException, IOException {
		XSSFWorkbook wb = new XSSFWorkbook(f);
		XSSFSheet ws = wb.getSheet(shtName);

		int rowCount = ws.getLastRowNum() - ws.getFirstRowNum();
		Object arr[][] = new Object[rowCount][];

		for (int i = 0; i < rowCount; i++) {
			Row r = ws.getRow(i);
			int cellCount = r.getLastCellNum();
			arr[i] = new Object[cellCount];
			System.out.println("total column are " + i);
			for (int j = 0; j < cellCount; j++) {
//				
				CellType c = r.getCell(j).getCellType();
				if(c==CellType.STRING) {
					arr[i][j]=r.getCell(j).getStringCellValue();
				}else if(c==CellType.NUMERIC) {
					arr[i][j]=r.getCell(j).getNumericCellValue();
				}
				System.out.println(arr[i][j]);
			}

		}
		return arr;

	}

}
