import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class MultiSelect {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(
				"https://www.softwaretestingmaterial.com/sample-webpage-to-automate/");

		WebElement we = driver.findElement(By.xpath("(//select[@class='spTextField'])[1]"));
		Select sel = new Select(we);
		sel.selectByValue("msmanual");
		sel.selectByIndex(3);
		sel.selectByVisibleText("Performance Testing");

		List<WebElement> lst = sel.getOptions();

		for (int i = 0; i < lst.size(); i++) {
			System.out.println(lst.get(i).getText());
		}

		
		
		
	}
}
