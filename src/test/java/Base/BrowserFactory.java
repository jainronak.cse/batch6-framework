package Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserFactory {

	
	
	private BrowserFactory() {
		
	}
	
	public static WebDriver createInstance(String browserName) {
		WebDriver driver=null;

		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver\\chromedriver.exe");
			driver = new ChromeDriver();
			System.out.println("chrome browser object will created");
		}
		
		return driver;
	}
	
	
}
