package Base;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentReportUtil {

	public static ExtentReports extent;
	

	public static void initReports() {
		if (Objects.isNull(extent)) {
			extent = new ExtentReports();
			ExtentSparkReporter spark = new ExtentSparkReporter(".//automation.html");
			final File CONF = new File(
					"C:\\Users\\dell\\NewEclipse\\Batch6FrameWork\\src\\test\\resources\\sparkconfig.xml");

			try {
				spark.loadXMLConfig(CONF);
			} catch (IOException e) {
				e.printStackTrace();
			}
			extent.attachReporter(spark);
		}
	}

	public static void tearDownExtentReports() {
		if(Objects.nonNull(extent)) {
		extent.flush();
		
		}
		
	}

	public static void createTestExtent(String testName) {
		ExtentTest test =extent.createTest(testName);
		ExtentFacory.setExtTest(test);
		
	}

}
