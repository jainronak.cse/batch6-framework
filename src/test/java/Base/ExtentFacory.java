package Base;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentTest;

public class ExtentFacory {

	private  static ThreadLocal<ExtentTest> extTest = new ThreadLocal<ExtentTest>();
	
	
	public static ExtentTest getExtTest() {
		return extTest.get();
	}
	public static void setExtTest(ExtentTest test) {
		 extTest.set(test);
	}
	
	public static void unload() {
		extTest.remove();
	}
	
}
