package Base;

import org.openqa.grid.internal.BaseGridRegistry;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.MediaEntityBuilder;

public class ListnerTestNg implements ITestListener {

	public void onFinish(ITestContext arg0) {

		System.out.println("finishing all the method");

	}

	public void onStart(ITestContext arg0) {
		
		System.out.println("starting all the methods");

	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult res) {
		System.out.println("onTestFailedButWithinSuccessPercentage" + res.getName());

	}

	public void onTestFailure(ITestResult res) {
		System.out.println("onTestFailure " + res.getName());
		ExtentFacory.getExtTest().pass("test case is pass rounak",MediaEntityBuilder.createScreenCaptureFromBase64String(BaseClass.getBase64()).build());

	}

	public void onTestSkipped(ITestResult arg0) {
		System.out.println("onTestSkipped :" + arg0.getName());

	}

	public void onTestStart(ITestResult it) {

		ExtentReportUtil.createTestExtent(it.getMethod().getMethodName());
		System.out.println("on test start :" + it.getName());

	}

	public void onTestSuccess(ITestResult it) {
		ExtentFacory.getExtTest().pass(it.getMethod().getMethodName() + " is passed");
		System.out.println("on the success " + it.getName());

	}

}
