package Base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


import Utilities.BrowserBasics;

public class BaseClass {

	public static Properties prop;

	public static BrowserBasics bb;
	private static ThreadLocal<WebDriver> drivers = new ThreadLocal<WebDriver>();

	public BaseClass() {
		FileInputStream fis;
		File f = new File(".//config.properties");
		try {
			fis = new FileInputStream(f);
			prop = new Properties();
			prop.load(fis);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@BeforeSuite
	public void initRe() {
		System.out.println("in intire");
		ExtentReportUtil.initReports();
	}
	
	@AfterSuite
	public void closeRe() {
		System.out.println("in closere");
		ExtentReportUtil.tearDownExtentReports();
	}
	
	
	public void init() {
		String bName = prop.getProperty("browser");
		WebDriver driver = BrowserFactory.createInstance(bName);
		drivers.set(driver);

//		if (bName.equalsIgnoreCase("chrome")) {
//			System.setProperty("webdriver.chrome.driver",
//					System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver\\chromedriver.exe");
//			driver = new ChromeDriver();
//		}
		getDriver().get(prop.getProperty("url"));
		getDriver().manage().window().maximize();
		initUtility();
	}

	public static WebDriver getDriver() {
		return drivers.get();
	}

	public void initUtility() {
		bb = new BrowserBasics();

	}

	public void tearDown() {
		getDriver().quit();
	}

	public  static String getBase64() {
		return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BASE64);
	}
}
