import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandleInJava {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.seleniumeasy.com/window-popup-modal-demo.html");
//		driver.findElement(By.xpath("//a[text()='  Follow On Twitter ']")).click();
		driver.findElement(By.id("followall")).click();
		String parentId = driver.getWindowHandle();
		Set<String> win = driver.getWindowHandles();
		System.out.println(win.size());
		Iterator<String> itr = win.iterator();
		String twinid = null;
		while (itr.hasNext()) {
			String cid = itr.next();
			driver.switchTo().window(cid);
			String d = driver.getCurrentUrl();

			if (d.contains("twitter")) {
				twinid = cid;
			} else {
				driver.close();
			}
		}
		driver.switchTo().window(twinid);
		System.out.println(driver.getCurrentUrl());
//		driver.switchTo().window(parentId);
//		System.out.println(driver.getCurrentUrl());
	}
}
