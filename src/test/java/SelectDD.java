import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectDD {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.testfire.net/login.jsp");
		driver.findElement(By.id("uid")).sendKeys("jsmith");
		driver.findElement(By.name("passw")).sendKeys("demo1234");
		driver.findElement(By.name("btnSubmit")).click();
		WebElement we = driver.findElement(By.id("listAccounts"));
		Select sel = new Select(we);
		sel.selectByValue("800003");
		
		List<WebElement> lst = sel.getOptions();
		
		for(int i=0;i<lst.size();i++) {
			System.out.println(lst.get(i).getText());
		}
		
	}

}
