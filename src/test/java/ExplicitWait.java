import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExplicitWait {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir")+"\\src\\test\\resources\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.testfire.net/login.jsp");
		
		WebDriverWait wait= new WebDriverWait(driver, 10);
		
		WebElement w = wait.until(ExpectedConditions.presenceOfElementLocated((By.id("uid1"))));
		w.sendKeys("jsmith");
		
		
	}
}
