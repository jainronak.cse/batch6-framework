package TestNG;

import org.testng.annotations.Test;

import Utilities.ExcelUtility;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

public class DataProviderTest {

	@BeforeMethod()
	public void beforeMethod() {
		System.out.println("in before method");
	}

	@Test(dataProvider = "dp")
	public void f(Object a,Object b) {

//		System.out.println(n);
		System.out.println(a);
		System.out.println(b);
	}

	@DataProvider
	public Object[][] dp() throws InvalidFormatException, IOException {

//		return new Object[][] { new Object[] { 1, "a" ,"rounak"}, new Object[] { 2, "b" }, };
		ExcelUtility e= new ExcelUtility("C:\\Users\\dell\\NewEclipse\\Batch6FrameWork\\TestData.xlsx");
//		String s =e.returnCellData("Sheet1", 4, 1);
//		System.out.println(s);
		
		Object arr[][] =e.returnAllData("Sheet1");
	return arr;
	}
	
	
	@Test
	public void readData() throws InvalidFormatException, IOException {
		ExcelUtility e= new ExcelUtility("C:\\Users\\dell\\NewEclipse\\Batch6FrameWork\\TestData.xlsx");
//	String s =e.returnCellData("Sheet1", 4, 1);
//	System.out.println(s);
	
	Object arr[][] =e.returnAllData("Sheet1");
//	for(int i=0;i<arr.length;i++) {
//		for(int j=0;j<arr[i].length;j++) {
//			System.out.println(arr[i][j]);
//		}
//	}
//	return arr;
	}
}
