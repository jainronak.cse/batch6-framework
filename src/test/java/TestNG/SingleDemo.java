package TestNG;

public class SingleDemo {

	
	
	public static void main(String[] args) {
		
		SingletonPattern obj1= SingletonPattern.getIns();
		SingletonPattern obj2= SingletonPattern.getIns();
		SingletonPattern obj3= SingletonPattern.getIns();
		SingletonPattern obj4= SingletonPattern.getIns();
		
		System.out.println(obj1.hashCode());
		System.out.println(obj2.hashCode());
		System.out.println(obj3.hashCode());
		System.out.println(obj4.hashCode());
		
		int i=10;
		SingletonPattern.show(i);
	}
}
