package TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class FirstTestNG {
//	@Test
	public void f() {
		System.out.println("in test");
	}
	@Test
	public void f2() {
		System.out.println("in test 2");
	}
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("in before method");
	}

	@AfterMethod
	public void afterMethod() {

		System.out.println("in afterMethod method");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("in beforeClass method");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("in afterClass method");
	}

	@BeforeTest
	public void beforeTest() {
		System.out.println("in beforeTest method");
	}

	@AfterTest
	public void AfterTest() {
		System.out.println("in AfterTest method");
	}

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("in beforeSuite method");
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("in afterSuite method");
	}

}
