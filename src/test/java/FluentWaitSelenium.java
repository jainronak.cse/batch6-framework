import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class FluentWaitSelenium {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\test\\resources\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.testfire.net/login.jsp");
		FluentWait w = new FluentWait(driver).withTimeout(5000, TimeUnit.MILLISECONDS).pollingEvery(1000, TimeUnit.MILLISECONDS).ignoring(Exception.class);

	 w.until(ExpectedConditions.presenceOfElementLocated((By.id("uid1"))));
		driver.findElement(By.id("uid1")).sendKeys("jsmith");
	}
}
