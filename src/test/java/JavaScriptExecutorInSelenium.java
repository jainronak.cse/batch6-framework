import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class JavaScriptExecutorInSelenium {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir")+"\\src\\test\\resources\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.testfire.net/login.jsp");
		WebElement we = driver.findElement(By.id("uid"));
		we.sendKeys("jsmith");
		driver.findElement(By.name("passw")).sendKeys("demo1234");
		WebElement wsubs =driver.findElement(By.name("btnSubmit"));
		
		JavascriptExecutor js= (JavascriptExecutor) driver;
		
//		js.executeScript("arguments[0].click();", wsubs);
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",wsubs);
		
	}
}
