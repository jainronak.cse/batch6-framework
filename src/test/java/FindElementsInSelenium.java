import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElementsInSelenium {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir")+"\\src\\test\\resources\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.testfire.net/login.jsp");
		List<WebElement> we =driver.findElements(By.tagName("a"));
		
		for(int i=0;i<we.size();i++) {
//			System.out.println(we.get(i).getText());
			System.out.println(we.get(i).getAttribute("href"));
		}
	}
}
