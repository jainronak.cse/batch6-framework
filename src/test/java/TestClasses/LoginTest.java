package TestClasses;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import Base.BaseClass;
import Base.ExtentFacory;
import Base.ListnerTestNg;
import PageClasses.LoginPage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.IOException;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;

//@Listeners(ListnerTestNg.class)
public class LoginTest extends BaseClass {
	LoginPage lp;

	
	@BeforeMethod
	public void intialize() throws IOException {
		
		init();
		lp = new LoginPage();

	}

	@Test(description = "This test case is to verify if a user is able to login successfully")
	public void tc_001() {
		String uname = prop.getProperty("username");
		String password = prop.getProperty("password");
		lp.login(uname, password);
		ExtentFacory.getExtTest().info("I am executing valid Login");
		Assert.assertTrue(lp.verifyLogin());
		
	}

	//@Test
	public void tc_002() {
		lp.login("rounak", "j");
	
		Assert.assertTrue(lp.verifyWarning(), "Test case failed: As user is not able to see the warning message");
		
	}

	 @AfterMethod
	public void kill() {
		tearDown();
	}

}
