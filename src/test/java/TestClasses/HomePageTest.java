package TestClasses;

import org.testng.annotations.Test;

import Base.BaseClass;
import PageClasses.HomePage;
import PageClasses.LoginPage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class HomePageTest extends BaseClass {

	LoginPage lp;
	HomePage hp;
	@BeforeMethod
	public void beforeMethod() {
		init();
		lp = new LoginPage();
		String uname = prop.getProperty("username");
		String password = prop.getProperty("password");
		lp.login(uname, password);
	}

	@Test
	public void tc_004() {
	}

	@AfterMethod
	public void afterMethod() {
	}

}
