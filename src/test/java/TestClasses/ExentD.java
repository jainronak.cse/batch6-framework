package TestClasses;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import Base.BaseClass;
import Base.ListnerTestNg;
import PageClasses.LoginPage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.IOException;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;

//@Listeners(ListnerTestNg.class)
public class ExentD extends BaseClass {
	LoginPage lp;

	ExtentReports extent = new ExtentReports();;
	ExtentSparkReporter spark = new ExtentSparkReporter(".//automation.html");
	ExtentSparkReporter spark1 = new ExtentSparkReporter(".//failedtestcase.html").filter().statusFilter().as(new Status [] {Status.FAIL}).apply();
	final File CONF = new File("C:\\Users\\dell\\NewEclipse\\Batch6FrameWork\\src\\test\\resources\\sparkconfig.xml");
	
	@BeforeMethod
	public void intialize() throws IOException {
		spark1.loadXMLConfig(CONF);
		spark.loadXMLConfig(CONF);
		init();
		lp = new LoginPage();
//		spark.config().setTheme(Theme.STANDARD);
//		spark.config().setDocumentTitle("Jain_Rounak_LNCT");
//
//		spark.getConf().setReportName("Automation test report");
//
		extent.attachReporter(spark,spark1);
	}

	@Test(description = "This test case is to verify if a user is able to login successfully")
	public void tc_001() {

		ExtentTest test = extent.createTest("verifyLogin").assignDevice("windows").assignAuthor("rounak")
				.assignCategory("smoke");

		String uname = prop.getProperty("username");
		String password = prop.getProperty("password");
		lp.login(uname, password);
		test.pass("test case is pass rounak",MediaEntityBuilder.createScreenCaptureFromBase64String(getBase64()).build());
		test.info("login test info");
		Assert.assertTrue(lp.verifyLogin());
		extent.flush();
	}

	@Test
	public void tc_002() {
		lp.login("rounak", "j");
		ExtentTest test = extent.createTest("verifyLogin warning").assignDevice("windows").assignAuthor("shruti")
				.assignCategory("sanity");
		test.fail("tc02 is fail");
		Assert.assertTrue(lp.verifyWarning(), "Test case failed: As user is not able to see the warning message");
		extent.flush();
	}

	 @AfterMethod
	public void kill() {
		tearDown();
	}

}
