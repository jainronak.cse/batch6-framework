package TestClasses;

import org.testng.annotations.Test;

import Base.BaseClass;
import PageClasses.LoginPage;

import org.testng.annotations.BeforeMethod;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class LoggerDemo extends BaseClass {
	LoginPage lp;

//	public static Logger l = Logger.getLogger(LoggerDemo.class);
	public static Logger l1= Logger.getLogger("devpinoyLogger");
	@BeforeMethod
	public void intialize() {
		init();
		lp = new LoginPage();
	}

	@Test(description = "This test case is to verify if a user is able to login successfully")
	public void tc_001() {
//		l.debug("This is our first test case");
//		l1.error("debug error");
		String uname = prop.getProperty("username");
		String password = prop.getProperty("password");
		lp.login(uname, password);
		Assert.assertTrue(lp.verifyLogin());
		
	}

	@Test
	public void tc_002() {
		lp.login("rounak", "j");
		Assert.assertTrue(lp.verifyWarning(), "Test case failed: As user is not able to see the warning message");
	}

	//@AfterMethod
	public void kill() {
		tearDown();
	}

}
